using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinCollider : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        GameManager.UpdateScore();
    }
}
