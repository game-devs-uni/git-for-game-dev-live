﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    
    int score = 0;
    public static int Score { get { return instance.score; } set { instance.score = value; } } 

    void Awake()
    {
        instance = this;
    }

    public static void UpdateScore()
    {
        instance.score++;
    }
}
